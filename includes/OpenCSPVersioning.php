<?php

namespace CSPResources;

/**
 * @package CSPResources
 */
class OpenCSPVersioning {

	/**
	 * @return array
	 */
	public static function getVersion(): array {
		$extension = json_decode( file_get_contents( __DIR__ . DIRECTORY_SEPARATOR . "../extension.json" ),
			true );

		return [ '[https://www.open-csp.org Open CSP]' => $extension['version'] ];
	}
}