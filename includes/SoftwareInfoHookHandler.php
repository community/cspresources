<?php

namespace CSPResources;

/**
 * CSPResources MediaWiki extension
 * Copyright (C) 2023 open-csp.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


/**
 * Class SoftwareInfoHookHandler
 *
 * @package CSPResources
 */
class SoftwareInfoHookHandler {
	/**
	 * This hook is called by Special:Version for returning information about the software.
	 *
	 * @param array &$software The array of software in format 'name' => 'version'. See
	 *   SpecialVersion::softwareInformation().
	 * @return void True or no return value to continue or false to abort
	 * @since 1.35
	 */
	public static function onSoftwareInfo( array &$software ) {
		$software = array_merge( $software, OpenCSPVersioning::getVersion() );
	}
}
