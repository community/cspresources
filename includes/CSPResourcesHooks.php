<?php

namespace CSPResources;

use MediaWiki\MediaWikiServices;

/**
 * This class has any hooks that may be required to make the Resources load for CSP correctly and effectively
 * through the resource loader.
 */
class CSPResourcesHooks {
	/**
	 * Just before loading the page, load any conditional stylesheets that we may want to use.
	 *
	 * @param OutputPage $out the page that is loaded
	 * @param Skin $skin the skin that is used
	 *
	 * @return void no return is given
	 */
	public static function onBeforePageDisplay( \OutputPage $out, \Skin $skin ) {
		// Load default css and js files.
		$out->addModuleStyles( 'ext.CSPResources.css' );
		$out->addModules( 'ext.CSPResources.js' );

		// Conditionally hide or show the SMW Indicator
		if ( MediaWikiServices::getInstance()->getMainConfig()->has( "CSPShowSmwIndicator" ) ) {
			$showIndicator = MediaWikiServices::getInstance()->getMainConfig()->get( "CSPShowSmwIndicator" );
			if ( $showIndicator ) {
				$out->addModuleStyles( 'ext.CSPResources.indicator_fixed' );
			} else {
				self::hideIndicator( $out );
			}
		} else {
			self::hideIndicator( $out );
		}
	}

	/**
	 * If the indicator is to be hidden, do whatever is required to hide it.
	 *
	 * @param \OutputPage $out the page that is edited
	 *
	 * @return void no return is given
	 */
	public static function hideIndicator( \OutputPage $out ) {
		$out->addModuleStyles( 'ext.CSPResources.indicator_hidden' );
	}
}

