/**
 * adds a insert snippet tool to VE
 */
var urlParams = new URLSearchParams( window.location.search );

if ( urlParams.has( "veaction" ) ) {
	var windowManager = false;

	mw.loader
		.using( "ext.visualEditor.desktopArticleTarget.init" )
		.then( function () {
			mw.loader.using( ["ext.visualEditor.core"] ).then( function () {
				/**
				 * TOOL
				 *
				 * @param toolGroup
				 * @param config
				 */
				ve.ui.snippetTool = function snippetTool( toolGroup, config ) {
					ve.ui.snippetTool.super.apply( this, arguments );
					this.setDisabled( false );
				};
				OO.inheritClass( ve.ui.snippetTool, ve.ui.Tool );
				ve.ui.snippetTool.static.name = "snippet";
				ve.ui.snippetTool.static.icon = "wikiText";
				ve.ui.snippetTool.static.displayBothIconAndLabel = true;
				ve.ui.snippetTool.static.autoAddToGroup = true;
				ve.ui.snippetTool.static.autoAddToCatchall = true;
				ve.ui.snippetTool.static.title = "Snippet";
				ve.ui.snippetTool.prototype.onSelect = function () {
					if ( !windowManager ) {
						windowManager = new OO.ui.WindowManager();

						snippetDialog = new SnippetDialog();
						$( document.body ).append( windowManager.$element );
						windowManager.addWindows( [snippetDialog] );
						windowManager.openWindow( snippetDialog );
					} else {
						snippetDialog.open();
					}
					this.setActive( false );
				};
				ve.ui.snippetTool.prototype.onUpdateState = function () {
					ve.ui.snippetTool.super.prototype.onUpdateState.apply(
						this,
						arguments
					);
				};
				ve.ui.toolFactory.register( ve.ui.snippetTool );

				/**
				 *  DIALOG
				 *
				 * @param config
				 */

				function SnippetDialog( config ) {
					SnippetDialog.super.call( this, config );
				}

				OO.inheritClass( SnippetDialog, OO.ui.ProcessDialog );

				SnippetDialog.static.name = "SnippetDialog";
				SnippetDialog.static.title = "Add a snippet";
				SnippetDialog.static.actions = [
					{
						flags: ["primary", "progressive"],
						label: "Insert",
						action: "open",
						disabled: true,
					},
					{
						flags: "safe",
						label: "Cancel",
					},
				];

				SnippetDialog.prototype.initialize = function () {
					var dialog = this;

					SnippetDialog.super.prototype.initialize.call( this );
					this.panel = new OO.ui.PanelLayout( {
						padded: true,
						expanded: false,
					} );
					this.content = new OO.ui.FieldsetLayout();

					this.input = new OO.ui.ComboBoxInputWidget( {
						placeholder: "Search for a snippet",
						menu: {
							filterFromInput: true,
						},
						validate: function ( val ) {
							if ( !val ) {
								dialog.actions.categorized.actions.open[0].setDisabled( true );
								return false;
							}

							if ( !this.menu.findItemFromData( val ) ) {
								dialog.actions.categorized.actions.open[0].setDisabled( true );
								return false;
							}
							dialog.actions.categorized.actions.open[0].setDisabled( false );
							return true;
						},
					} );

					this.field = new OO.ui.FieldLayout( this.input, {
						label: "Snippet",
						align: "top",
					} );

					this.content.addItems( [this.field] );
					this.panel.$element.append( this.content.$element );
					this.$body.append( this.panel.$element );
				};

				SnippetDialog.prototype.getBodyHeight = function () {
					return this.panel.$element.outerHeight( true ) + 300;
				};

				SnippetDialog.prototype.getSetupProcess = function ( data ) {
					data = data || {};
					return SnippetDialog.super.prototype.getSetupProcess
						.call( this, data )
						.next( function () {
							var dialog = this;
							dialog.input.setValue( [] );
							var params = {
									action: "ask",
									query: "[[Class::Snippet]]|?Title|limit=500",
									formatversion: 2,
									format: "json",
								},
								api = new mw.Api();

							api.post( params ).done( function ( data ) {
								var options = [];
								if ( data.query ) {
									Object.entries( data.query.results ).forEach( function ( entrie ) {
										options.push( {
											data: entrie[0],
											label: entrie[1].printouts.Title.length
												? entrie[1].printouts.Title[0]
												: entrie[0],
										} );
									} );
									dialog.input.setOptions( options );
								}
							} );
						}, this );
				};

				var revisionContent = "";

				SnippetDialog.prototype.getActionProcess = function ( action ) {
					var dialog = this;
					if ( action === "open" ) {
						return new OO.ui.Process( function () {
							page = dialog.input.getValue();

							var params = {
									action: "query",
									prop: "revisions",
									titles: page,
									rvprop: "content",
									rvslots: "main",
									formatversion: "2",
									format: "json",
								},
								api = new mw.Api();
							return api.post( params ).done( function ( data ) {
								revisionContent =
									data.query.pages[0].revisions[0].slots.main.content;
							} );
						}, this )
							.next( function () {
								var surfaceModel = ve.init.target.getSurface().getModel();

								var selection = surfaceModel.getSelection();

								var range = selection.getRange();
								var selectedRange = new ve.Range( range.from, range.to );

								var fragment = surfaceModel.getLinearFragment( selectedRange );

								var params = {
									action: "visualeditor",
									page: "test",
									paction: "parsefragment",
									format: "json",
									wikitext: revisionContent,
								};
								var api = new mw.Api();

								return api.post( params ).done( function ( data ) {
									fragment.insertHtml( data.visualeditor.content );
								} );
							} )
							.next( function () {
								dialog.close();
							} );
					}
					return SnippetDialog.super.prototype.getActionProcess.call(
						this,
						action
					);
				};
			} );
		} );
}
