var urlParams = new URLSearchParams(window.location.search);

if (urlParams.has("veaction")) {
  mw.loader
    .using([
      "ext.visualEditor.core",
      "ext.visualEditor.mwtransclusion",
      "ext.visualEditor.desktopArticleTarget",
    ])
    .then(function () {
        /**
         * Modifies link tool
         * 
         * @method overrides ve.ui.MWInternalLinkAnnotationWidget.prototype.createInputWidget
         */
        // var nspaces = [];
        ve.ui.MWInternalLinkAnnotationWidget.prototype.createInputWidget =
        function (config) {
            var input = new mw.widgets.TitleSearchWidget(
                ve.extendObject(
                    {
                        icon: "search",
                        excludeCurrentPage: true,
                        showImages: true,
                        showDescriptions: true,
                        showInterwikis: true,
                        addQueryInput: false,
                        api: ve.init.target.getContentApi(),
                        cache: ve.init.platform.linkCache,
                    },
                    config
                )
            );

            input.$element.prepend(input.$query);

            input.getApiParams = function (query) {
                var params = {
                    action: "ask",
                    query: "[[Class::+]]|limit=500",
                    format: "json",
                    formatversion: 2,
                }

                return params;
            };

            input.getSuggestionsPromise = function () {
            var api = this.getApi(),
            query = this.getQueryValue(),
            widget = this,
            promiseAbortObject = { abort: function () {
                // Do nothing. This is just so OOUI doesn't break due to abort being undefined.
            } };

            var params = {
                action: "ask",
                query: "[[Class::+]][[in:" + query + "]]|?Image url|?Home|limit=10|sort=es.score|order=desc",
                format: "json",
                formatversion: 2,
            }

            if ( !mw.Title.newFromText( query ) ) {
                // Don't send invalid titles to the API.
                // Just pretend it returned nothing so we can show the 'invalid title' section
                return $.Deferred().resolve( {} ).promise( promiseAbortObject );
            }
            return this.getInterwikiPrefixesPromise().then( function ( interwikiPrefixes ) {
            // Optimization: check we have any prefixes.
            if ( interwikiPrefixes.length ) {
                var interwiki = query.slice( 0, Math.max( 0, query.indexOf( ':' ) ) );
                if (
                    interwiki !== '' &&
                    interwikiPrefixes.indexOf( interwiki ) !== -1
                ) {
                    // Interwiki prefix is valid: return the original query as a valid title
                    // NB: This doesn't check if the title actually exists on the other wiki
                    return $.Deferred().resolve( { query: {
                        pages: [ {
                            title: query
                        } ]
                    } } ).promise( promiseAbortObject );
                }
            }
            // Not an interwiki: do a prefix-search API lookup of the query.
            var prefixSearchRequest = api.get( params );
            promiseAbortObject.abort = prefixSearchRequest.abort.bind( prefixSearchRequest ); // TODO ew
            return prefixSearchRequest.then( function ( prefixSearchResponse ) {
                return prefixSearchResponse;
            } );
        } ).promise( promiseAbortObject );
    };

        input.getOptionsFromData = function (data) {
            if (!data.results) {
            return [];
            }
            var that = this;
            var items = Object.entries(data.results).map(function([key, value]){
                return that.createOptionWidget(
                    that.getOptionWidgetData(key, {
                        description: value.displaytitle
                    })
                )
        });
        return items;
        };

        // Remove 'maxlength', because it should not apply to full URLs, which we allow users to paste
        // here. Maximum length of page titles will still be enforced by JS validation later (we can't
        // override maxLength config option, because that would break the validation).
        input.getQuery().$input.removeAttr("maxlength");

        input.query.$input.attr(
            "aria-label",
            mw.msg("visualeditor-linkinspector-button-link-internal")
        );
        return input;
    };})
}