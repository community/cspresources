# CSPResources

Adds styling and javascript to the CSP projects.

CSPResources has settings you can do:

* <code>$wgCSPShowSmwIndicator</code>: Whether to show the semantic mediawiki indicator for loading. If unchanged, will
  show it in a fixed position. If set to false, will be hidden. _(Default: false)_